package com.cs487.oad.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;



/**
 * Created by Jeannius on 11/26/2016.
 */
@Controller
public class IndexController {

    @RequestMapping( "/" )
    public RedirectView redirectWithUsingRedirectView(RedirectAttributes attributes,
                                                      @RequestParam(value = "query", required = false) String query,
                                                      @RequestParam(value = "city", required = false) String city,
                                                      @RequestParam(value = "neighborhood", required = false) String neighborhood) {
	
	attributes.addAttribute("query", query);
	attributes.addAttribute("city", city);
	attributes.addAttribute("neighborhood", neighborhood);
        return new RedirectView("index.html");
    }

    @RequestMapping( value = "/admin")
    public RedirectView redirectWithUsingRedirectViewadmin(RedirectAttributes attributes) {
        return new RedirectView("administrator.html");
    }

}
