var url = "";

//SET FIND OBJECT
function setFindObject(object){
	findObject = object;	
}

//TEST OBJECT
function testFindGetObject(){
	var testObject = 
		{"locations":[
			{"city":"Chicago",
			"neighborhoods":["Magnificent Mile", "Chicago Loop", "Wicker Park", "Chinatown", "Logan Square", "Lincoln Park"]
			},
			{"city":"Schaumburg",
			"neighborhoods":[]
			},
			{"city":"Evanston",
			"neighborhoods":["Skokie","Wilmette","Highland"]
			}]
		}		
	return testObject;
}

var findObject;
//POPULATE FIND CITY LIST
function populateFindCityList(data){
		findObject = data;
		
		var cityList = document.getElementById("city_drop_list");
		
		for (var i = 0; i < findObject.locations.length; i++){
			var cityOption = document.createElement("option");
			var city = findObject.locations[i].city;
			
			cityOption.textContent = city;
			cityOption.value = city;
			cityList.appendChild(cityOption);
		}
}

//POPULATE FIND NEIGHBORHOOD LIST
function populateFindNeighborhoodList(value){
	$("#neighborhood_drop_list").empty();
	var neighborhoodList = document.getElementById("neighborhood_drop_list");
	var defaultOption = document.createElement("option");
	defaultOption.selected = true;
	defaultOption.disabled = true;
	defaultOption.textContent = "Select neighborhood...";
	neighborhoodList.appendChild(defaultOption);
	
	for (var i = 0; i < findObject.locations[value].neighborhoods.length; i++){
		var neighborhoodOption = document.createElement("option");
		var neighborhood = findObject.locations[value].neighborhoods[i];
		
		neighborhoodOption.textContent = neighborhood;
		neighborhoodOption.value = neighborhood;
		neighborhoodList.appendChild(neighborhoodOption);	
	}
	
}

//SET QUERY
function setQuery(queryString){
	var listingSearchBox = document.getElementById("listing_search");
	listingSearchBox.value = queryString;
}

//SET CITY
function setCity(cityString){
	var cityList = document.getElementById("city_drop_list");
	cityList.value = cityString;
	console.log(cityList.selectedIndex-1);
	populateFindNeighborhoodList(cityList.selectedIndex-1);
	
}

//SET NEIGHBORHOOD
function setNeighborhood(neighborhoodString){
	var neighborhoodList = document.getElementById("neighborhood_drop_list");
	neighborhoodList.value = neighborhoodString;
	console.log("set neighborhood");
}

//FIND LISTINGS
function findListings(){
	
	var listing = document.getElementById("listing_search").value;
	var city = document.getElementById("city_drop_list").value;
	var neighborhood = document.getElementById("neighborhood_drop_list").value;
	
	/*	
	//Ensures query is passed
	if (listing === ""){
		document.getElementById("listing_search").style.border = "5px solid #FF0000";
		return;
	}
	*/	
	
    //ADD EXCEPTIONS FOR PLACEHOLDER TEXT
    if (city === "Select your location..."){ city = ""};
    if (neighborhood === "Select neighborhood..."){neighborhood = ""};
    
	$.get(url+"/listing/search?query=" + listing + "&city=" + city + "&neighborhood=" + neighborhood, function(data, status){
		$(categories).empty();
		$(home_featured_listing).empty();
		$(featured_list).empty();
        	var uniqueCategoryObject = makeUnique(data);
		createHTML2(uniqueCategoryObject);
	});
}

//ENSURE CATEGORY UNIQUENESS
function removeDuplicates(arr, prop) {
     var new_arr = [];
     var lookup  = {};
 
     for (var i in arr) {
         lookup[arr[i][prop]] = arr[i];
     }
 
     for (i in lookup) {
         new_arr.push(lookup[i]);
     }
 
     return new_arr;
 }

function makeUnique(jsonObject){
    jsonObject.categories = removeDuplicates(jsonObject.categories, "name")
    return jsonObject;
}

//PAGE LOAD
function pageLoad(){
	$.get(url+"/location", function(data, status){
        	populateFindCityList(data);
    });
    
	document.getElementById("search_button").addEventListener("click", findListings);
}

window.onload = pageLoad();
